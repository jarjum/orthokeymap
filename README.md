# Personal 40% ortholinear keymap

This is the keymap for my 40% ortholinear NIU keyboard from kbdfans.myshopify.com.
The layout is designed with a gaming / programming role in mind:

- Brackets and special characters needed in coding on base, lower, or raise layer.
- Special layers for MOBAs, FPS, MMOs, and arrowkey-reliant games.

![Layout](layout.png)
